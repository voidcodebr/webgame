from django.shortcuts import render
from WebGame.models import Player, Map, Npc, Mob

def start(request):
    players = Player.object.filter(id=1).all()
    maps = Map.object.filter(id=1).all()
    context = {
        'players': players,
        'maps': maps
    }
    page = "game.html"

    return render(
        request,
        page,
        context
    )
def update(request):

    book= get_object_or_404(Player, pk=pk)
    form = Player(request.POST or None, instance=book)

    page = "game.html"

    if form.is_valid():
        form.save()
        return redirect('game')
    return render(request, page, {'form':form})
