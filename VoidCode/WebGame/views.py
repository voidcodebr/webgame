from django.shortcuts import render
from django_ajax.decorators import ajax

def my_view(request):
    c = 2 + 3
    return {'result': c}
