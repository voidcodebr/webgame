from django.db import models

class Player(models.Model):

    name = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    life = models.IntegerField(
        default = 100
    )

    mana = models.IntegerField(
        default = 100
    )

    stamina = models.IntegerField(
        default = 100
    )

    classes = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    description = models.CharField(
        max_length = 255,
        blank = False,
        null = False,
        default = "nada"
    )

    X = models.IntegerField(
        default = 0
    )

    Y = models.IntegerField(
        default = 0
    )

    map = models.IntegerField(
        default = 1
    )

    race = models.CharField(
        max_length = 255,
        blank = False,
        null = False,
        default = "teste"
    )

    date = models.DateField(
        default = '0000-00-00'
    )

    object = models.Manager()

class Map(models.Model):

    name = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    X = models.IntegerField(
        default = 0
    )

    Y = models.IntegerField(
        default = 0
    )

    wall = models.CharField(
        max_length = 625,
        default = "0",
        blank = False,
        null = False
    )

    history = models.CharField(
        default = "Nada",
        max_length = 255
    )

    description = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    date = models.DateField(
        default = '0000-00-00'
    )

    object = models.Manager()

class Npc(models.Model):

    name = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    description = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    object = models.Manager()

class Mob(models.Model):

    name = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    description = models.CharField(
        max_length = 255,
        blank = False,
        null = False
    )

    object = models.Manager()
