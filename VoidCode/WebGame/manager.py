from django.shortcuts import render
from WebGame.models import Player, Map, Npc, Mob

def startManagerPlayers(request):
    player = Player.object.all()

    list = {
        'players': player
    }
    return render(
        request,
        "manager.html",
        list
    )

def startManagerNpcs(request):
    data = Npc.object.all()
    list = {
        'data': data
    }
    return render(
        request,
        "manager.html",
        list
    )

def startManagerMobs(request):
    data = Mob.object.all()
    list = {
        'data': data
    }
    return render(
        request,
        "manager.html",
        list
    )

def startManagerMaps(request):
    #Max X = 8
    #Max Y = 5
    maps = Map.object.all()
    list = {
        'maps': maps
    }

    return render(
        request,
        "manager.html",
        list
    )
